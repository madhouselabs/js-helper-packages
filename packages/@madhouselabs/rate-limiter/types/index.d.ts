export function useRateLimiter(app: any, { redis_url: redisUrl, redis_prefix: redisPrefix, req_per_sec: reqPerSec, chooseConsumer, }: {
    redis_url: String;
    redis_prefix: String;
    req_per_sec: Number;
    chooseConsumer: ((req) => String) ;
}): void;
