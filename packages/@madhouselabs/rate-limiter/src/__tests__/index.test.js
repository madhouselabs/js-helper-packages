import { filterRequest } from '../index';

describe('@madhouselabs/rate-limiter', () => {
  // test("", () => {
  //   const app = {
  //     use: jest.fn(),
  //   };
  //   // This is the input  api of the function,
  //   useRateLimiter(app, {
  //     redis_url: 'any url',
  //     redis_prefix: 'any prefix',
  //     req_per_sec: anyNumber,
  //     chooseConsumer: (req) => jest.fn(),
  //   })
  // })

  // Output API of the function
  test.todo(
    'respond with a boolean, whether the request should go to next middleware or not'
  );
  test('respond with true and false, on conditions', () => {
    const req = {};
    expect(filterRequest(req, 2)).toBeTruthy();
    expect(filterRequest(req, 0)).toBeFalsy();
  });

  // Input API
  test('setting MAX LIMITS', () => {
    setupRateLimiter({
      req_per_sec: 5,
    });
  });
});
