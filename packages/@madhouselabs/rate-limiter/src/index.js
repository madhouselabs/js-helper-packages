// import redis from 'redis';
// import { RateLimiterRedis } from 'rate-limiter-flexible';
// import { StatusCodes } from 'http-status-codes';
// import createError from 'http-errors-lite';
//
// export const useRateLimiter = (
//   app,
//   {
//     redis_url: redisUrl,
//     redis_prefix: redisPrefix,
//     req_per_sec: reqPerSec,
//     chooseConsumer,
//   }
// ) => {
//   const redisClient = redis.createClient(redisUrl.split('?')[0]);
//   const rateLimiter = new RateLimiterRedis({
//     storeClient: redisClient,
//     keyPrefix: redisPrefix,
//     points: reqPerSec,
//     duration: 1,
//   });
//
//   const middleware = async (req, _res, next) => {
//     try {
//       await rateLimiter.consume(chooseConsumer(req));
//       next();
//     } catch (err) {
//       next(
//         createError(
//           StatusCodes.TOO_MANY_REQUESTS,
//           'Too Many Requests, try in a few seconds ...'
//         )
//       );
//     }
//   };
//   app.use(middleware);
// };

export const rateLimiter = {
  setupRateLimiter: () => {},
};

export const filterRequest = (req, limits) => {
  if (limits) return true;
  return false;
};

export const setupRateLimiter = {
  req_per_sec: reqPerSec,
};
