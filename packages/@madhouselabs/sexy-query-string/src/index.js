const flatObj = (obj,result={}, mainKey) => {
    Object.entries(obj).forEach(([key, value]) => {
        let nextKey;
        if(mainKey){
            nextKey = `${mainKey}[${key}]`;
        }else {
            nextKey = key;
        }
        if (typeof value === "object") {
            flatObj(value,result, nextKey);
        } else {
            result[nextKey] = value;
        }
    });
    return result;
}

const buildQueryString = (qObj) => {
    return Object.entries(flatObj(qObj)).map(([key, value])=>{
        return `${key}=${encodeURI(value)}`
    }).join("&");
};

module.exports = buildQueryString;
