import { Client } from '@elastic/elasticsearch';

// eslint-disable-next-line import/prefer-default-export
export const createElasticClient = (urls) =>
  new Client({
    node: urls,
    log: 'trace',
  });

Client.prototype.ensureIndex = async function ({ index, mapping }) {
  const exists = await this.indices.exists({ index });
  if (exists.body === false) {
    await this.indices.create({
      index,
      body: {
        mappings: {
          properties: {
            ...mapping,
          },
        },
      },
    });
  }
};

Client.prototype.addRecord = async function ({
  index,
  record_id: recordId,
  record,
  mapping_type: mappingType,
}) {
  return this.index({
    index,
    type: mappingType,
    id: recordId,
    body: {
      ...record,
      is_deleted: false,
    },
  });
};

Client.prototype.deleteById = function ({
  index,
  record_id: recordId,
  are_you_sure: areYouSure = false,
}) {
  if (areYouSure) {
    return this.delete({
      index,
      id: recordId,
      refresh: true,
    });
  }
  return this.update({
    index,
    id: recordId,
    refresh: true,
    body: {
      doc: {
        is_deleted: true,
      },
    },
  });
};

Client.prototype.updateById = async function ({
  index,
  record_id: recordId,
  data,
}) {
  return this.update({
    index,
    id: recordId,
    refresh: true,
    body: {
      doc: {
        ...data,
      },
    },
  });
};

Client.prototype.doQuery = async function ({
  index,
  mapping_type: mappingType,
  query,
  sort,
  page = { offset: 0, size: 10 },
  aggs,
}) {
  return this.search({
    index,
    type: mappingType,
    body: {
      ...query,
      ...aggs,
      ...sort,
    },
    from: page.offset,
    size: page.size,
  });
};

Client.prototype.findById = async function ({
  index,
  record_id,
  mapping_type: mappingType,
}) {
  return this.get({
    index,
    type: mappingType,
    id: record_id,
  });
};
