import crypto from 'crypto';
import assert from 'assert';

const ALGORITHM = 'aes-256-cbc';

export const encrypt = (text, key) => {
  const xKey = crypto.createHash('md5').update(key).digest('hex');
  const iv = crypto.randomBytes(16);
  const cipher = crypto.createCipheriv(ALGORITHM, Buffer.from(xKey), iv);
  let encrypted = cipher.update(text);
  encrypted = Buffer.concat([encrypted, cipher.final()]);
  return JSON.stringify({
    iv: iv.toString('hex'),
    encryptedData: encrypted.toString('hex'),
  });
};

export const decrypt = (text, key) => {
  const xKey = crypto.createHash('md5').update(key).digest('hex');
  try {
    const obj = JSON.parse(text);
    assert(Object.keys(obj).length > 0, 'No JSON Object received');

    const iv = Buffer.from(obj.iv, 'hex');
    const encryptedText = Buffer.from(obj.encryptedData, 'hex');
    const decipher = crypto.createDecipheriv(ALGORITHM, Buffer.from(xKey), iv);
    let decrypted = decipher.update(encryptedText);
    decrypted = Buffer.concat([decrypted, decipher.final()]);
    return decrypted.toString();
  } catch (err) {
    // console.warn(`Text: ${text} is not JSON parseable`);
    return null;
  }
};
