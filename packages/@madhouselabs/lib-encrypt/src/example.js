import { encrypt, decrypt } from './index';

const x = encrypt(
  '{"DB_URL": "mongodb://localhost:27107/lokal"}',
  process.env.SECRET_KEY
);
console.log(x);

let y = decrypt(x, process.env.SECRET_KEY);
console.log(y);

y = decrypt('assdkfjasdklfj', process.env.SECRET_KEY);
console.log(y);

y = decrypt('', process.env.SECRET_KEY);
console.log(y);

y = decrypt(undefined, process.env.SECRET_KEY);
console.log(y);

y = decrypt(null, process.env.SECRET_KEY);
console.log(y);

y = decrypt(0, process.env.SECRET_KEY);
console.log(y);
