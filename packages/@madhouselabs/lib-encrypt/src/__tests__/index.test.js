import {decrypt, encrypt} from '../index';

// jest.mock('crypto', () => {
//   return {
//     randomBytes: jest.fn(),
//   };
// });

describe('lib encrypt says', () => {
  const key = 'sample';
  test('encryption and decryption works', async () => {
    const text = 'sample';
    const eText = encrypt('sample', key);
    expect(eText).not.toBe(text);
    const dText = decrypt(eText, key);
    expect(dText).toBe(text);
  });
});
