import createError from 'http-errors-lite';
import { StatusCodes } from 'http-status-codes';
import { isProduction } from './what-is-my-env';

const notFoundHandler = (req, res, next) => {
  next(createError(StatusCodes.NOT_FOUND, `${req.originalUrl} is not found`));
};

const errorHandler = (err, req, res, _next) => {
  Logger.error(err.message);
  err.statusCode = err.statusCode || 500;
  const canDisplayBadError = err.statusCode < 500 || !isProduction;
  res
    .status(err.statusCode || 500)
    .send(canDisplayBadError ? err.message : 'Something bad happened');
};

const useErrorHandler = (app) => {
  app.use(notFoundHandler);
  app.use(errorHandler);
};

export default useErrorHandler;
