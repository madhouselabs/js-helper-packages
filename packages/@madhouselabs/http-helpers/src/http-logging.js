import './logger';
import expressPinoLogger from 'express-pino-logger';

const useHttpLogging = (app) => {
  app.use(
    expressPinoLogger({
      logger: Logger,
      serializers: {
        req: (req) => ({
          method: req.method,
          url: req.url,
          body: req.raw.body,
        }),
      },
      res: (res) => ({
        statusCode: res.statusCode,
        headers: res.headers,
        body: res.raw.body,
      }),
    })
  );
};

export default useHttpLogging;
