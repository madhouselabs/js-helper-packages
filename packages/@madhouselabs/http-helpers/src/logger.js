import pino from 'pino';

const devLogger = pino({ level: 'debug' });
const logger = pino();

const isDevelopment =
  'NODE_ENV' in process.env && process.env.NODE_ENV === 'development';

if (isDevelopment) devLogger.debug('Logger configured for DEV Environment');

global.Logger = isDevelopment ? devLogger : logger;
