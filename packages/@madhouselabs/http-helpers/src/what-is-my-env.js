export const isProduction =
  (process.env.NODE_ENV || '').toLowerCase() === 'production';

export const isDevelopment =
  !process.env.NODE_ENV || process.env.NODE_ENV.toLowerCase() === 'development';
