import { encrypt } from '@madhouselabs/lib-encrypt';
import { init } from './index';

function makeCipher() {
  const toCipher = {
    db_url: 'mongodb://production:27017/lokal-auth',
    session: {
      redis_url: 'redis://localhost:6379?prefix=lokal',
      secret:
        'VnBHZv0bOycEj8XodmQiJFDB0uTjd5fPntJ0bO4rwXpSW5rWyC8h1s8Oc37Nxz6PZJ5Fz1Ig5xr83ob0emYQvA',
    },
    jwks: {},
  };

  const cipherText = encrypt(JSON.stringify(toCipher), 'sample');
  console.log(cipherText);
}

function decipher() {
  (async () => {
    const secrets = await init({
      zookeeper_url: 'localhost:2181',
      decryption_key: 'sample',
      path: '/com-lokalbits/secrets',
      defaults: {
        db_url: 'mongodb://localhost:27017/lokal-auth',
        session: {
          redis_url: 'redis://localhost:6379?prefix=lokal',
          secret:
            'VnBHZv0bOycEj8XodmQiJFDB0uTjd5fPntJ0bO4rwXpSW5rWyC8h1s8Oc37Nxz6PZJ5Fz1Ig5xr83ob0emYQvA',
        },
        jwks: {},
      },
      // skipZooKeeper: process.env.NODE_ENV === 'development',
      skip_zookeeper: false,
    });

    secrets.keepWatching((srt) => {
      console.log('SRT: ', srt);
    });

    secrets.on('jwks', (val) => {
      console.log('jwks: ', val);
    });
  })();
}

decipher();
// makeCipher()
