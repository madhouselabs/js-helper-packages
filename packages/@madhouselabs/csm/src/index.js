import zookeeper from 'node-zookeeper-client';
import { decrypt } from '@madhouselabs/lib-encrypt';
import '@madhouselabs/http-helpers';
import EventEmitter from 'events';

const connectZookeeper = async (zookeeperUrl) => {
  const client = zookeeper.createClient(zookeeperUrl);
  return new Promise((resolve, reject) => {
    client.on('connected', (err) => {
      if (err) {
        Logger.error('Failed to connect to zookeeper');
        reject(err);
        return;
      }
      Logger.info('Successfully connected to zookeeper');
      resolve(client);
    });

    client.connect();
  });
};

const zoo = new EventEmitter();
const listeners = new Set();

const readZNode = async (client, zNode, decryptionKey) => {
  return new Promise((resolve, reject) => {
    client.getData(
      zNode,
      (_event) => {
        readZNode(client, zNode, decryptionKey);
      },
      (err, data) => {
        if (err) {
          reject(err);
          return;
        }
        if (!decryptionKey) throw new Error('No Decryption key provided');
        const decData = decrypt(data.toString(), decryptionKey);
        if (!decData)
          throw new Error(
            `[Secrets]: Failed to decrypt zNode [${zNode}] with the provided decryption key`
          );
        const _data = JSON.parse(decData);
        zoo.emit('secrets', _data);

        listeners.forEach((key) => {
          zoo.emit(key, _data[key]);
        });

        resolve({
          ..._data,
          keepWatching: (fn) => {
            zoo.on('secrets', (value) => {
              fn(value);
            });
          },
          on: (key, fn) => {
            listeners.add(key);
            zoo.on(key, (value) => {
              fn(value);
            });
          },
        });
      }
    );
  });
};

export const init = async ({
  zookeeper_url: zookeeperUrl,
  decryption_key: decryptionKey,
  path: zNode,
  defaults,
  skip_zookeeper: skipZookeeper,
}) => {
  if (skipZookeeper) {
    return Promise.resolve({
      ...defaults,
      keepWatching: (fn) => {
        fn(defaults);
      },
      on: (key, fn) => {
        fn(defaults[key]);
      },
    });
  }

  const client = await connectZookeeper(zookeeperUrl);
  return readZNode(client, zNode, decryptionKey);
};
