export function init({ zookeeper_url, decryption_key , path, defaults, skip_zookeeper }: {
    zookeeper_url: any;
    decryption_key: any;
    path: any;
    defaults: any;
    skip_zookeeper: any;
}): Promise<any>;
