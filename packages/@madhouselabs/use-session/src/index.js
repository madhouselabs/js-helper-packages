import redis from 'redis';
import connectRedis from 'connect-redis';
import session from 'express-session';

export const useSession = (
  app,
  {
    session: { redis_url: redisUrl, secret },
    cookie: { max_age: maxAge, domain },
  }
) => {
  const RedisStore = connectRedis(session);
  const redisClient = redis.createClient(redisUrl);
  app.use(
    session({
      store: new RedisStore({ client: redisClient }),
      secret,
      resave: false,
      saveUninitialized: true,
      cookie: {
        httpOnly: true,
        maxAge,
        secure: true,
        sameSite: 'strict',
        domain,
      },
    })
  );
};
