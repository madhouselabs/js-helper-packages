export function useSession(app: any, { session: { redis_url: redisUrl, secret }, cookie: { max_age: maxAge, domain }, }: {
    session: {
        redis_url: String;
        secret: String;
    };
    cookie: {
        max_age: Number;
        domain: String;
    };
}): void;
