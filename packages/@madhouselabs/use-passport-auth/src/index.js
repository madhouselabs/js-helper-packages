import passport, { Passport } from 'passport';
import { Strategy as LocalStrategy } from 'passport-local';
import createError from 'http-errors-lite';
import { StatusCodes } from 'http-status-codes';
import '@madhouselabs/http-helpers';
import { Router } from 'express';

LocalStrategy.prototype.authenticate = function (req) {
  const self = this;

  function done(err, user, info) {
    if (err) return self.error(err);
    if (!user) return self.fail(info);
    return self.success(user, info);
  }

  try {
    this._verify(req, done);
  } catch (ex) {
    return self.error(ex);
  }
};

const initPassport = (p) => {
  p.serializeUser((user, done) => {
    done(null, user);
  });

  p.deserializeUser((user, done) => {
    (async () => {
      try {
        done(null, user);
      } catch (err) {
        done(err);
      }
    })();
  });
};

export const setupPassportAuth = ({
  sessionCfg,
  actions: { verifyLogin, getCredentials },
}) => {
  passport.use(
    new LocalStrategy(
      {
        passReqToCallback: true,
      },
      (req, done) => {
        (async () => {
          try {
            const user = await verifyLogin(getCredentials(req));
            done(null, user);
          } catch (err) {
            done(err);
          }
        })();
      }
    )
  );

  initPassport(passport);
  const router = Router();
  router.use(sessionCfg);
  router.use(passport.initialize());
  router.use(passport.session());

  return router;
};

export const usePassportAuth = (sessionCfg) => {
  const p = new Passport();
  initPassport(p);
  const router = Router();
  router.use(sessionCfg);
  router.use(p.initialize());
  router.use(p.session());
  return router;
};

export const doLogin = (req, res, next) => {
  passport.authenticate('local', (err, user, _info) => {
    if (err) return next(err);
    req.logIn(user, (err2) => {
      if (err2) next(err2);
    });
    return next();
  })(req, res, next);
};

export const isLoggedIn = (req, res, next) => {
  if (req.isAuthenticated()) return next();
  return next(createError(StatusCodes.UNAUTHORIZED, 'User is not logged in'));
};
