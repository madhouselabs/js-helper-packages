import createError from 'http-errors-lite';
import mockedRedis from 'redis';import mockedConnectRedis from 'connect-redis';import mockedExpressSession from 'express-session';
import mockedPassport from 'passport';
import { Strategy as MockedLocalStrategy } from 'passport-local';

import { StatusCodes } from 'http-status-codes';
import { usePassportAuth, doLogin, isLoggedIn } from '../index';

jest.mock('passport');

jest.mock('connect-redis', () => {
  return jest.fn();
});

jest.mock('express-session', () => {
  return jest.fn(() => 'sample-session-mw');
});

jest.mock('redis', () => {
  return {
    createClient: jest.fn(),
  };
});

jest.mock('passport-local', () => {
  return {
    Strategy: jest.fn(),
  };
});

describe('app session should work', () => {
  const serializeUserCb = jest.fn();
  const deserializeUserCb = jest.fn();
  const stratCb = jest.fn();
  beforeEach(() => {
    jest.clearAllMocks();
  });

  beforeAll(() => {
    mockedConnectRedis.mockImplementation(() => {
      return () => {
        return {
          hi: 'helo',
        };
      };
    });

    mockedPassport.initialize.mockImplementation(() => {
      return 'passport-initialize-mw';
    });
    mockedPassport.session.mockImplementation(() => {
      return 'passport-session-mw';
    });
    mockedPassport.serializeUser.mockImplementation((callback) => {
      callback({ _id: 'sampleId' }, serializeUserCb);
    });
    mockedPassport.deserializeUser.mockImplementation((callback) => {
      callback('sampleId', deserializeUserCb);
    });
    MockedLocalStrategy.mockImplementation((conf, cb) => {
      cb({ my: 'request' }, stratCb);
      return {
        mocked: 'mocked-local-strat',
      };
    });
  });

  test('session add should work', (done) => {
    const app = {
      use: jest.fn(),
    };
    const config = {
      fields: { username: 'username', password: 'password' },
      session: { redis_url: 'redisUrl', secret: 'secret' },
      cookie: {
        domain: 'domain',
        max_age: 'maxAge',
        path: 'cookiePath',
        same_site: 'sameSite',
        secure: 'secure',
      },
      actions: {
        verifyLogin: jest.fn(),
        getCredentials: jest.fn(() => 'dummy credientials'),
        findByUserId: jest.fn(() => {
          return new Promise((resolve) => resolve('dummy-user'));
        }),
      },
    };
    usePassportAuth(app, config);
    expect(mockedPassport.serializeUser).toHaveBeenCalled();
    expect(serializeUserCb).toHaveBeenCalledWith(null, 'sampleId');
    expect(mockedPassport.deserializeUser).toHaveBeenCalled();
    expect(config.actions.findByUserId).toHaveBeenCalledWith('sampleId');
    expect(MockedLocalStrategy).toHaveBeenCalledWith(
      {
        passReqToCallback: true,
      },
      expect.anything()
    );
    expect(mockedPassport.use).toHaveBeenCalledWith({
      mocked: 'mocked-local-strat',
    });
    expect(config.actions.verifyLogin).toHaveBeenCalledWith(
      'dummy credientials'
    );
    expect(config.actions.getCredentials).toHaveBeenCalledWith({
      my: 'request',
    });
    setTimeout(() => {
      expect(deserializeUserCb).toHaveBeenCalledWith(null, 'dummy-user');
      done();
    }, 0);
    expect(mockedRedis.createClient).toHaveBeenCalledWith(
      config.session.redis_url
    );
    expect(mockedExpressSession).toHaveBeenCalledWith(
      expect.objectContaining({
        store: {
          hi: 'helo',
        },
        secret: config.session.secret,
        saveUninitialized: true,
        resave: false,
        cookie: {
          path: config.cookie.path,
          domain: config.cookie.domain,
          maxAge: config.cookie.max_age || 3 * 60 * 60 * 1000,
          sameSite: config.cookie.same_site || 'strict',
          secure: config.cookie.secure || false,
        },
      })
    );
    expect(app.use).toHaveBeenCalledWith('sample-session-mw');
    expect(app.use).toHaveBeenCalledWith('passport-initialize-mw');
    expect(app.use).toHaveBeenCalledWith('passport-session-mw');
  });

  test('session add should throw', (done) => {
    const app = {
      use: jest.fn(),
    };
    const config = {
      fields: { username: 'username', password: 'password' },
      session: { redis_url: 'redisUrl', secret: 'secret' },
      cookie: {
        domain: 'domain',
        max_age: 'maxAge',
        path: 'cookiePath',
        same_site: 'sameSite',
        secure: 'secure',
      },
      actions: {
        verifyLogin: jest.fn(),
        findByUserId: jest.fn(() => {
          return new Promise((resolve, reject) => reject('dummy-error'));
        }),
      },
    };
    usePassportAuth(app, config);
    setTimeout(() => {
      expect(deserializeUserCb).toHaveBeenCalledWith('dummy-error');
      done();
    }, 0);
  });
});

describe('sample', () => {
  test('checkForAuth', async () => {
    const authenticateFn = jest.fn();
    mockedPassport.authenticate.mockImplementation((_, callback) => {
      callback(null, 'user', 'info');
      return authenticateFn;
    });
    const req = {
      logIn: jest.fn(),
    };
    const next = jest.fn();
    doLogin(req, '', next);
    expect(mockedPassport.authenticate).toHaveBeenCalledWith(
      'local',
      expect.anything()
    );
    expect(authenticateFn).toHaveBeenCalledWith(req, '', next);
    expect(req.logIn).toHaveBeenCalledWith('user', expect.anything());
    expect(next).toHaveBeenCalled();
  });
  test('checkForAuth2', async () => {
    const authenticateFn = jest.fn();
    mockedPassport.authenticate.mockImplementation((_, callback) => {
      callback('err', 'user', 'info');
      return authenticateFn;
    });
    const req = {
      logIn: jest.fn(),
    };
    const next = jest.fn();
    doLogin(req, '', next);
    expect(mockedPassport.authenticate).toHaveBeenCalledWith(
      'local',
      expect.anything()
    );
    expect(authenticateFn).toHaveBeenCalledWith(req, '', next);
    expect(req.logIn).not.toHaveBeenCalled();
    expect(next).toHaveBeenCalledWith('err');
  });
  test('checkForAuth3', async () => {
    const authenticateFn = jest.fn();
    mockedPassport.authenticate.mockImplementation((_, callback) => {
      callback(null, 'user', 'info');
      return authenticateFn;
    });
    const req = {
      logIn: jest.fn((usr, callback) => {
        callback('err');
      }),
    };
    const next = jest.fn();
    doLogin(req, '', next);
    expect(req.logIn).toHaveBeenCalledWith('user', expect.anything());
    expect(next).toHaveBeenCalledWith('err');
  });
});

describe('isLoggedIn', () => {
  test('1', () => {
    const req = {
      isAuthenticated: () => true,
    };
    const next = jest.fn();
    isLoggedIn(req, 'res', next);
    expect(next).toHaveBeenCalledWith();
  });
  test('2', () => {
    const req = {
      isAuthenticated: () => false,
    };
    const next = jest.fn();
    isLoggedIn(req, 'res', next);
    expect(next).toHaveBeenCalledWith(
      createError(StatusCodes.UNAUTHORIZED, 'User is not logged in')
    );
  });
});
