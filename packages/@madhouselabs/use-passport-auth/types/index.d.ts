export function setupPassportAuth({ sessionCfg, actions: { verifyLogin, getCredentials }, }: {
    sessionCfg: any;
    actions: {
        verifyLogin: any;
        getCredentials: any;
    };
}): any;
export function usePassportAuth(sessionCfg: any): any;
export function doLogin(req: any, res: any, next: any): void;
export function isLoggedIn(req: any, res: any, next: any): any;
