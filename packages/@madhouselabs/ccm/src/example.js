import { init } from './index';

function verify() {
  (async () => {
    const config = await init({
      zookeeper_url: 'localhost:2181',
      path: '/com-lokalbits/configs',
      defaults: {
        cookie: {
          domain: 'localhost.com',
          max_age: 24 * 60 * 60 * 1000,
          path: '/',
          http_only: true,
          same_site: 'strict',
          secure: false,
        },
        cors_regex: '[.]*localhost[.]com$',
        n_req_per_sec: 10,
      },
      // skipZooKeeper: process.env.NODE_ENV === 'development',
      skip_zookeeper: false,
    });
    console.log('Config: ', config);

    config.keepWatching((cfg) => {
      console.log('Cfg: ', cfg);
    });

    config.on('cors_regex', (val) => {
      console.log('cors_regex is', val);
    });
  })();
}

function setConfig() {
  const x = {
    cookie: {
      domain: 'nxtcoder17.me',
      max_age: 24 * 60 * 60 * 1000,
      path: '/',
      http_only: true,
      same_site: 'strict',
      secure: false,
    },
    cors_regex: '[.]*localhost[.]com$',
    n_req_per_sec: 10,
  };

  console.log(JSON.stringify(x));
}

// setConfig();
verify();
