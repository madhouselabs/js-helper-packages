import zookeeper from 'node-zookeeper-client';
import '@madhouselabs/http-helpers';
import EventEmitter from 'events';

const connectZookeeper = async (zookeeperUrl) => {
  const client = zookeeper.createClient(zookeeperUrl);
  return new Promise((resolve, reject) => {
    client.on('connected', (err) => {
      if (err) {
        Logger.error('Failed to connect to zookeeper');
        reject(err);
        return;
      }
      Logger.info('Successfully connected to zookeeper');
      resolve(client);
    });

    client.connect();
  });
};

const zoo = new EventEmitter();
const listeners = new Set();

const readZNode = async (client, zNode) => {
  return new Promise((resolve, reject) => {
    client.getData(
      zNode,
      (_event) => {
        readZNode(client, zNode);
      },
      (err, data) => {
        if (err) {
          reject(err);
          return;
        }
        const _data = JSON.parse(data);

        zoo.emit('config', _data);

        listeners.forEach((key) => {
          zoo.emit(key, _data[key]);
        });

        resolve({
          ..._data,
          keepWatching: (fn) => {
            zoo.on('config', (value) => {
              fn(value);
            });
          },
          on: (key, fn) => {
            listeners.add(key);
            zoo.on(key, (value) => {
              fn(value);
            });
          },
        });
      }
    );
  });
};

export const init = async ({
  zookeeper_url: zookeeperUrl,
  path: zNode,
  defaults,
  skip_zookeeper: skipZookeeper,
}) => {
  if (skipZookeeper) {
    return Promise.resolve({
      ...defaults,
      keepWatching: (fn) => {
        fn(defaults);
      },
      on: (key, fn) => {
        fn(defaults[key]);
      },
    });
  }

  const client = await connectZookeeper(zookeeperUrl);
  return readZNode(client, zNode);
};
