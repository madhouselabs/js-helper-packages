import mockedZookeeper from 'node-zookeeper-client';
import EventEmitter from 'events';
import { init, getConfig } from '../index';

const mockedClient = new EventEmitter();

jest.mock('node-zookeeper-client', () => {
  return {
    createClient: jest.fn(() => {
      mockedClient.connect = jest.fn();
      mockedClient.getData = jest.fn();
      return mockedClient;
    }),
  };
});

jest.useFakeTimers();

describe('@madhouselabs/ccm', () => {
  let config;
  beforeAll(() => {
    config = {
      zookeeper_url: 'sample',
      path: '/sample',
      defaults: {
        hello: 'world',
      },
      skip_zookeeper: false,
    };
  });

  test('connects to zookeeper', async () => {
    init(config);
    expect(mockedZookeeper.createClient).toHaveBeenCalledWith(
      config.zookeeper_url
    );
    mockedClient.emit('connected');
    expect(mockedClient.connect).toBeCalled();
  });

  test('should read data from zookeeper', async () => {
    init(config);
    expect(mockedZookeeper.createClient).toHaveBeenCalledWith(
      config.zookeeper_url
    );
    mockedClient.emit('connected');
  });

  // test('client should read data from zookeeper', async () => {
  //   expect.assertions(1);
  //   await init(config);
  //   mockedClient.emit('connected');
  //   mockedClient.getData.mockImplementationOnce((zNode, watcherFn, fn) => {
  //     setTimeout(() => {
  //       fn(null, { x: 'y' });
  //     }, 0);
  //   });
  //   jest.runAllTimers();
  //
  //   const x = getConfig();
  //   console.log(x);
  //
  //   expect(x).toBe({
  //     x: 'y',
  //   });
  // });
});
