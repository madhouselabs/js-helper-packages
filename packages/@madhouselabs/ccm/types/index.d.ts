export function init({ zookeeper_url, path, defaults, skip_zookeeper }: {
    zookeeper_url: any;
    path: any;
    defaults: any;
    skip_zookeeper: any;
}): Promise<any>;
