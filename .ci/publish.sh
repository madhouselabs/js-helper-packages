#!/usr/bin/env bash
CWD=$(pwd)

npm i -g pnpm
pnpm i -P
for DIFF_PATH in $(git diff-tree --no-commit-id --name-only -r $CI_COMMIT_SHA | grep package.json)
  do
    DIR=$(dirname $DIFF_PATH)
    if [ $DIR != $CWD ]
    then
      d=$CWD/$DIR
      cd $d
      pnpx babel --ignore '**/*.test.js' --root-mode=upward ./src -d ./dist

      echo "@madhouselabs:registry=https://gitlab.com/api/v4/packages/npm/" > ./.npmrc
      echo "//gitlab.com/api/v4/projects/${CI_PROJECT_ID}/packages/npm/:_authToken=${GITLAB_NPM_REGISTRY_TOKEN}" >> ./.npmrc
      
      cp $CWD/.ci/setup-package.js ./
      node $d/setup-package.js $CI_PROJECT_ID

      cp $CWD/.ci/.npmignore ./
      npx pnpm publish --no-git-checks

      cd $CWD
    fi
  done
