module.exports = {
  setupFilesAfterEnv: ['./.jest/setup.js'],
  transform: {
    '\\.[jt]sx?$': 'babel-jest',
  },
  reporters: ['default', 'jest-junit'],
};
