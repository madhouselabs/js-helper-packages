![coverage](https://gitlab.com/madhouselabs/js-helper-packages/badges/main/coverage.svg)


### `@madhouselabs/ccm`
***
makes easy to use Configurations from zookeeper, with first class support for `dev` mode for easy local development

This initialises CCM
```javascript
import {init} from "@madhouselabs/ccm"

(async () => {
  await init({
    zookeeper_url: 'localhost:2181',
    path: '/configs/sample-namespace',
    defaults: {
      cookie: {
        domain: 'localhost.com',
        httpOnly: true,
        samesite: 'strict',
      },
    },
    skip_zookeeper: process.env.RUNTIME_ENV === "local",
  });
})()
```

And, to use ccm
```javascript
import config from "@madhouslabs/ccm"

(async () => {
  await config.isReady()
  console.log("Cookie Domain is: ", config.cookie.domain)
})()
```

- `skip_zookeeper` toggles whether config would be from the `defaults` or from zookeeper itself

### `@madhouselabs/csm`
***
makes easy to consume encrypted secrets from your zookeeper instance,  with first class support for `dev` mode for easy local development

This initialises CSM
```javascript
import {init} from "@madhouselabs/csm"

(async () => {
  await init({
    zookeeper_url: 'localhost:2181',
    path: '/secrets/sample-namespace',
    decryption_key: process.env.DECRYPTION_KEY,
    defaults: {
      db_url: 'sadfjadkfjkdlsfhahdfjkh1232udf0p2341'
    },
    skip_zookeeper: process.env.RUNTIME_ENV === "local",
  });
})()
```

And, to use csm
```javascript
import secrets from "@madhouslabs/csm"

(async () => {
  await secrets.isReady();
  console.log("Cookie Domain is: ", secrets.db_url)
})()
```

- `secrets` object (the default export from `@madhouselabs/csm`) would have decrypted secrets at your disposal, ready to
  be consumed.
- `skip_zookeeper` toggles whether config would be from the `defaults` or from zookeeper itself
